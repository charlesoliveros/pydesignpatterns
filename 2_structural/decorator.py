from abc import ABC, abstractmethod

'''
Sencillo e interesante patrón que permite añadir funcionalidades a un objeto en aquellos 
casos en los que no sea necesario o recomendable hacerlo mediante herencia.

A continuación mostramos uno de los ejemplos clásicos usados para explicarlo: 
crear una clase que dibuje ventanas básicas, y utilizar el patrón para agregar 
barras de desplazamiento sólo a algunas de ellas.
'''

class WindowInterface(ABC):
    '''Clase Componente Interface'''

    @abstractmethod
    def draw(self, row: int, col: int) -> None:
        ...

class Window(WindowInterface):
    '''Clase Componente Concreto'''

    def draw(self, row: int, col: int) -> None:
        print(f'Dibujando ventana básica en [{row}] [{col}]')


class WindowDisplacementDecoratorAbstract(WindowInterface, ABC):
    '''Clase Decorador Abstracto'''

    window: WindowInterface

    def __init__(self, window: WindowInterface) -> None:
        self.window = window

    @abstractmethod
    def draw(self, row: int, col: int) -> None:
        ...


class WindowDisplacementHorizontalDecorator(WindowDisplacementDecoratorAbstract, ABC):
    '''Clase Decorador Concreto'''

    def draw(self, row: int, col: int) -> None:
        self.window.draw(row, col)
        self.add_displacement_horizontal()
    
    def add_displacement_horizontal(self) -> None:
        print('Barra de desplazamiento horizontal agregada')


class WindowDisplacementVerticalDecorator(WindowDisplacementDecoratorAbstract, ABC):
    '''Clase Decorador Concreto'''
    
    def draw(self, row: int, col: int) -> None:
        self.window.draw(row, col)
        self.add_displacement_vertical()
    
    def add_displacement_vertical(self) -> None:
        print('Barra de desplazamiento verfical agregada')


if __name__ == '__main__':
    window = Window()
    window.draw(row=50, col=100)

    print('--' * 20)

    '''Dibujar una ventana con barra de desplazamiento vertical'''
    window_with_displacement_hor =  WindowDisplacementHorizontalDecorator(Window())
    window_with_displacement_hor.draw(row=300, col=600)

    print('--' * 20)

    '''Dibujar una ventana con barra de desplazamiento vertical'''
    window_with_displacement_ver =  WindowDisplacementVerticalDecorator(Window())
    window_with_displacement_ver.draw(row=400, col=300)

    print('--' * 20)

    '''Dibujar una ventana con barra de desplazamiento vertical y horizontal'''
    window_with_displacement_hor_ver =  WindowDisplacementHorizontalDecorator(
        WindowDisplacementVerticalDecorator(Window())
        )
    window_with_displacement_hor_ver.draw(row=100, col=120)


'''
- La clase abstracta WindowDisplacementDecoratorAbstract es la encargada de definir las 
funcionalidades que se podrán agregar a objetos que (como ella misma) implementan la 
interface WindowInterface.

- Contiene además una referencia a otro objeto (asignada en el construtor) 
de tipo WindowInterface, y en el método draw() que debe implementar simplemente se llama 
al método del mismo nombre en dicho objeto.

- Los objetos que heredan de WindowDisplacementDecoratorAbstract llaman al constructor 
de su clase padre pasándole como parámetro el objeto de tipo WindowInterface recibido 
en su propio constructor (para guardar una referencia al mismo y así poder acceder a él 
en otro momento).

- Cuando se llama al método draw() de dichos objetos de tipo WindowDisplacementDecoratorAbstract 
se crea la ventana básica, y a continuación a un método propio para agregar la barra de 
desplazamiento deseada en cada caso.

- Al iniciarse el programa se crea una ventana básica y luego otras que contendrán 
una o ambas barras de desplazamiento según cada caso (observa cómo en el tercer caso 
se encadenan los contructores para agregar ambas barras de desplazamiento).
'''
