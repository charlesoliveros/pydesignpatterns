from abc import ABC, abstractmethod

'''
Según el libro de GoF este patrón de diseño permite desacoplar una abstracción de su 
implementación, de manera que ambas puedan variar de forma independiente.

Supongamos que tenemos una clase abstracta en la que se define un método que deberá 
implementar cada clase que herede de ella: ¿cómo haríamos si una clase hija necesitase 
implementarlo de forma que realizase acciones diferentes dependiendo de determinadas 
circunstancias?.

En dichos casos nos resultaría útil el patrón Bridge (puente) ya que 'desacopla una 
abstracción' (un método abstracto) al permitir indicar (durante la ejecución del programa) 
a una clase qué 'implementación' del mismo debe utilizar (qué acciones ha de realizar).

Veamos un ejemplo de uso de este patrón en el que creamos un sistema para elaborar lasagna, 
pudiendo ésta ser de carne o de verduras.
'''


class MakeFoodInterface(ABC):
    '''Implementador (Interface)'''

    @abstractmethod
    def process(self) -> None:
        ...

class LasagnaBeef(MakeFoodInterface):
    '''Implemetador concreto'''

    def process(self) -> None:
        print('Lasaña de carne elaborada')


class LasagnaVegetable(MakeFoodInterface):
    '''Implemetador concreto'''

    def process(self) -> None:
        print('Lasaña de vegetales elaborada')


class MakeFoodAbstract(ABC):
    '''Abstraccion'''
    
    implementer: MakeFoodInterface # referencia al implementador
    name: str

    @abstractmethod
    def getting(self) -> None:
        '''Método a implementar por las clases que hereden:
        Este es el metodo que cambia su comportamiento dependiendo de la implementación
        '''
        ...


class MakeLasagna(MakeFoodAbstract):
    '''Abstraccion Refinada'''

    def __init__(self, implementer: MakeFoodInterface) -> None:
        self.implementer = implementer

    def set_implementer(self, implementer: MakeFoodInterface) -> None:
        '''Este metodo permite cambiar la implementacion en tiempo de ejecición'''
        self.implementer = implementer

    def getting(self) -> None:
        print('Elaborando lasaña...')
        self.implementer.process()


if __name__ == '__main__':
    '''Crear un objeto de tipo "Abstraccion Refinada" indicándole un "Implementador Concreto"'''
    lasagna = MakeLasagna(LasagnaBeef())
    lasagna.getting()

    '''Ahora le indicamos que use otra implementación para obtener la de verduras'''
    lasagna.set_implementer(LasagnaVegetable())
    lasagna.getting()


'''
- En el programa principal se crea una instancia de MakeLasagna (de tipo Abstraccion Refinada) 
indicándole la implementación que debe usar, para luego utilizarla a la hora de elaborar 
el producto.

- A continuación se cambia de implementación y se obtiene otro producto.

- Como puedes observar, la clase abstracta MakeFoodAbstract (de tipo Abstraccion) aparte de 
definir un método que deberán implementar las que hereden de ella contiene una referencia a 
otra de tipo Implementador.

- En dicho método las clases hijas acceden al implementador (que puede ser cambiado en tiempo 
de ejecución), llamando al método definido en la interface MakeFoodInterface.
'''
