from abc import ABC, abstractmethod
from typing import List

'''
Este útil patrón permite crear y manejar estructuras de objetos en forma de árbol, 
en las que un objeto puede contener a otro(s).

En este punto cabe aclarar que las estructuras de este tipo se componen de nodes 
(un objeto que a su vez contiene otros objetos) y Hojas (objetos que no contienen otros), 
y que ambos comparten una misma Interface que define métodos que deben implementar.

Para ilustrar cómo funciona este patrón de diseño veamos a continuación un programa 
de ejemplo en el que gestionamos archivos y carpetas.
'''


class NodeAbstract(ABC):
    '''Componente'''

    FILE = 'FILE'
    FOLDER = 'FOLDER'

    name: str
    node_type: str

    def set_node_type(self, node_type: str) -> None:
        self.node_type = node_type
    
    @abstractmethod
    def show() -> None:
        '''Método a implementar por las clases que hereden'''
        ...


class NodeFile(NodeAbstract):
    '''Hoja'''

    def __init__(self, name: str) -> None:
        self.name = name
        self.node_type = self.FILE

    def show(self) -> None:
        print(f'Archivo: {self.name}')


class NodeFolder(NodeAbstract):
    '''Compuesto:
    Folder (Carpeta) puede tener otras carpetas o archivos (Files) que serian als hojas del
    arbol.
    '''

    nodes: List[NodeAbstract]

    def __init__(self, name: str) -> None:
        self.name = name
        self.node_type = self.FOLDER
        self.nodes = []
    
    def _get_type_node_spanish(self, node: NodeAbstract) -> str:
        if node.node_type == self.FOLDER:
            return 'Carpeta'
        return 'Archivo'
    
    def insert_node(self, node: NodeAbstract) -> None:
        self.nodes.append(node)
        node_type_spanish = self._get_type_node_spanish(node)
        print(f'{node_type_spanish} {node.name} agregado a carpeta {self.name}')
    
    def remove_node(self, node: NodeAbstract) -> None:
        self.nodes.remove(node)
        node_type_spanish = self._get_type_node_spanish(node)
        print(f'{node_type_spanish} {node.name} eliminado(a) de carpeta {self.name}')
    
    def get_nodes(self) -> List[NodeAbstract]:
        return self.nodes
    
    def get_node(self, position: int) -> NodeAbstract:
        self.nodes[position]
    
    def show(self) -> None:
        print(f'Listando carpeta: {self.name}')
        for node in self.nodes:
            node.show()


if __name__ == '__main__':

    '''Crear la carpeta principal e insertar archivos'''
    folder1 = NodeFolder('Carpeta1')
    folder1.insert_node(NodeFile('Archivo1.txt'))
    folder1.insert_node(NodeFile('Archivo2.txt'))
    folder1.insert_node(NodeFile('Archivo3.txt'))

    '''Crear una subcarpeta e insertar archivos'''
    folder2 = NodeFolder('Carpeta2')
    folder2.insert_node(NodeFile('Archivo4.txt'))
    folder2.insert_node(NodeFile('Archivo5.txt'))

    '''Insertar la subcarpeta dentro de la principal'''
    folder1.insert_node(folder2)

    '''Insertar otro archivo dentro de la carpeta principal'''
    folder1.insert_node(NodeFile('Archivo6.txt'))

    print('--' * 20)

    folder1.show()

    print('--' * 20)

    '''Eliminamos la subcarpeta (junto con su contenido)'''
    folder1.remove_node(folder2)
    folder1.show()
