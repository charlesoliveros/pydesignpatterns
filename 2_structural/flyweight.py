from abc import ABC, abstractmethod
from typing import List

'''
Este patrón resulta tremendamente útil para evitar crear un gran número de objetos similares, 
mejorando con ello el rendimiento de la aplicación.

No se trata de crear muchos objetos de forma dinámica, 
sino de crear sólo un objeto intermedio para cada entidad concreta.

Veamos un ejemplo clásico de uso de este patrón para que nos ayude a comprender 
cómo implementarlo: la creación de líneas de diferentes colores.
'''


class LightLineInterface(ABC):
    '''Clase Interface'''

    @abstractmethod
    def get_color(self) -> str:
        ...
    
    @abstractmethod
    def draw(self) -> None:
        ...

    
class LightLine(LightLineInterface):
    '''Clase concreta'''

    def __init__(self, color: str) -> None:
        self.color = color

    def get_color(self) -> str:
        return self.color
    
    def draw(self, row: int, col: int) -> None:
        print(f'Dibujando linea de color {self.color} en la fila {row} y columna {col}')


class FactoryLines:
    '''Clase tipo Factory y Singleton'''

    lines: List[LightLineInterface]

    def __init__(self) -> None:
        self.lines = []

    def get_line(self, color: str) -> LightLineInterface:
        for line in self.lines:
            '''Comprobar si hemos creado una línea con el color solicitado, 
            y devolverla en tal caso'''
            if line.get_color() == color:
                print(f'Linea de color {color} encontrada, la retornamos')
                return line
        
        '''Si no ha sido creada la creamos ahora, la agregamos a la lista y la devolvemos'''
        print(f'Creando linea de color {color}')
        line = LightLine(color=color)
        self.lines.append(line)
        return line


if __name__ == '__main__':
    factory_lines = FactoryLines()
    line_yellow = factory_lines.get_line('Amarillo')
    line_blue = factory_lines.get_line('Azul')
    line_red = factory_lines.get_line('Rojo')

    print('--'*50)

    line_yellow.draw(row=2, col=4)
    line_yellow.draw(row=4, col=6)
    line_blue.draw(row=5, col=10)
    line_blue.draw(row=10, col=15)
    line_yellow.draw(row=3, col=6)
    line_yellow.draw(row=6, col=9)


'''
- La clase FactoryLines guardará referencias a objetos LightLineInterface (los cuales son creados 
una sola vez por cada tipo de línea).

- Así según el ejemplo, cuando se desea dibujar una línea de color azul se crea un objeto 
LightLineInterface, pero cuando se quiere crear otra del mismo color se accede al objeto ya 
almacenado en la lista (observa que para ello hace uso del patrón Singleton).

- De este modo si por ejemplo se desean crear mil líneas... no es necesario crear un objeto 
para cada una de ellas, si no por cada tipo (por cada color).
'''
