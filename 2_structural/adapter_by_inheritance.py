from abc import ABC, abstractmethod


'''Este patrón permite que trabajen juntas clases con interfaces incompatibles.

Para ello, un objeto adaptador reenvía al otro objeto los datos que recibe 
(a través de los métodos que implementa, definidos en una clase abstracta o interface) 
tras manipularlos en caso necesario.'''

'''
Veamos un pequeño programa de ejemplo en el que utilizamos este patrón de diseño 
(usando Herencía), creando un objeto adaptador que convierte de pesos a euros para 
poder comunicarse con otro objeto que gestiona los movimientos de una caja en euros:
'''

'''Adapter por Herencia:

En este caso la clase Adaptador hereda de CashRegister (en lugar de contener una referencia 
a un objeto de dicho tipo como en el ejemplo anterior con la variable de 
clase cash_register_euros), para así poder acceder directamente a sus métodos.
'''

class CashRegisterEuros:
    '''Clase adaptable'''

    euros: float = 0

    def take_euros(self, euros: float) -> None:
        '''Petición concreta'''
        self.euros -= euros

    def add_euros(self, euros: float) -> None:
        '''Petición concreta'''
        self.euros += euros


class CashRegisterAdapterInterface(ABC):
    '''Clase Abstracta "Objetivo" o Adapter Interface:
    trabaja como un conversor a euros, y su moneda de origen depende de la implementacion
    '''

    def get_balance_euros(self) -> None:
        return self.euros

    @abstractmethod
    def _convert_to_euros(self, money: float) -> float:
        ...

    @abstractmethod
    def take_money(self) -> None:
        ...

    @abstractmethod
    def add_money(self) -> None:
        ...


class CashRegisterPesosEurosAdapter(CashRegisterAdapterInterface, CashRegisterEuros):
    '''Clase Adapter concreta'''

    def _convert_to_euros(self, pesos: float) -> float:
        return pesos / 4548

    def take_money(self, pesos: float) -> None:
        euros = self._convert_to_euros(pesos)
        self.take_euros(euros)

    def add_money(self, pesos: float) -> None:
        euros = self._convert_to_euros(pesos)
        self.add_euros(euros)
    
    
if __name__ == '__main__':
    cash_register_pesos_adapter = CashRegisterPesosEurosAdapter()
    cash_register_pesos_adapter.add_money(1000000)
    cash_register_pesos_adapter.add_money(1000000)
    cash_register_pesos_adapter.take_money(500000)

    print(f'Tottal euros: {cash_register_pesos_adapter.get_balance_euros()}')


'''
- La clase CashRegisterEuros contiene los métodos necesarios para ingresar 
y sacar fondos en euros.

- Creamos una interface (CashRegisterAdapterInterface) que define los métodos que 
implementará el objeto de tipo Adaptador, mediante los cuales éste redirigirá la 
comunicación hacia el objeto que gestiona la caja en euros 
(realizando previamente la conversión de pesetas a euros).

- Al inicio del programa se crea un objeto de tipo Adaptador, realizándose unos ingresos 
en pesos y obteniendo luego el saldo en euros.
'''
