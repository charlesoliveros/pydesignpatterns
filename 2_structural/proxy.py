from abc import ABC, abstractmethod
from time import sleep

'''
Este patrón se basa en proporcionar un objeto que haga de intermediario (proxy) de otro, 
para controlar el acceso a él.

Existen diferentes tipos de proxy:

- Proxy remoto: proporciona un representante local de un objeto situado en otro espacio 
de direcciones (en otro dispositivo conectado en red).
- Proxy virtual: usados para crear objetos costosos sólo cuando se soliciten.
- Proxy de protección: permiten controlar el acceso a un objeto cuando es accesible o no, 
dependiendo de determinados permisos.
- Referencia inteligente: un sustito de un puntero, que realiza operaciones adicionales 
en el momento de accederse al objeto.

Veamos un ejemplo de Proxy virtual en el que abrimos un documento que contiene una imagen, 
la cual se cargará y mostrará cuando el usuario haga scroll.
'''


class ImageInterface(ABC):
    '''Clase Sujeto Interface'''

    @abstractmethod
    def show_image(self) -> None:
        ...


class ImageReal(ImageInterface):
    '''Clase sujeto concreta'''

    name: str

    def __init__(self, name: str) -> None:
        self.name = name

    def show_image(self) -> None:
        print(f'Mostrando imagen: {self.name}')


class ImageProxy(ImageInterface):

    image_real: ImageReal = None
    name: str

    def __init__(self, name: str) -> None:
        self.name = name

    def show_image(self) -> None:
        if self.image_real is None:
            self.image_real = ImageReal(name=str(self.name))
        
        self.image_real.show_image()


class Document:

    doc_name: str
    text: str
    _image_proxy: ImageProxy

    def __init__(self, doc_name: str) -> None:
        self.doc_name = doc_name
        self.load_content()
        self.show_content()

    def load_content(self) -> None:
        '''Aquí cargaríamos el archivo y obtendríamos su contenido'''
        self.text = 'Texto del documento'
        self._image_proxy = ImageProxy('Imagen.jpg')
        print(f'Documento {self.doc_name} cargado en memoria')

    def show_content(self) -> None:
        print('Mostrando contenido...')

    def scroll(self) -> None:
        '''Supongamos que este método es un evento que se lanza al hacer scroll'''
        print('Haciendo Scroll..')
        self._image_proxy.show_image()


if __name__ == '__main__':
    document = Document(doc_name='mi_documento.docx')
    sleep(2)
    document.scroll()
