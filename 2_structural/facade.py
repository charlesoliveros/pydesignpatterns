'''
Este patrón nos permite acceder a un subsistema de forma más sencilla.

Para entenderlo mejor veamos un ejemplo sencillo en el que NO utilizaremos este patrón. 
Se trata de simular el proceso a realizar para arrancar un coche.
'''

class StartCar:

    def start(self) -> None:
        print('Introducimos la llave y le damos al encendido...')


class CheckMirror:

    def check(self) -> None:
        print('Comprobamos y regulamos los espejos retrovisores...')


class CheckSeat:

    def check(self) -> None:
        print('Comprobamos y regulamos el asiento...')


class CheckLiquids:

    def check(self) -> None:
        print('Comprobamos los liquidos de freno, agua, etc...')


if __name__ == '__main__':
    liquids = CheckLiquids()
    liquids.check()

    mirror = CheckMirror()
    mirror.check()

    seat = CheckSeat()
    seat.check()

    start_car = StartCar()
    start_car.start()

    print('---' * 20)

'''
como puedes observar, en este caso el programa principal (Main) es el encargado 
de acceder a cada uno de los objetos del subsistema creado con el objetivo de realizar 
el proceso de arrancar el coche.

Veamos ahora cómo quedaría utilizando el patrón de diseño Facade. 
Para ello modificamos el main y creamos otra que hará de fachada:

Usando patron Facade:
'''

class StartCarFacade:
    '''Clase Facade'''

    liquids: CheckLiquids
    mirror: CheckMirror
    seat: CheckSeat
    start_car: StartCar

    def __init__(self) -> None:
        self.liquids = CheckLiquids()
        self.mirror = CheckMirror()
        self.seat = CheckSeat()
        self.start_car = StartCar()
    
    def start(self) -> None:
        self.liquids.check()
        self.mirror.check()
        self.seat.check()
        self.start_car.start()


if __name__ == '__main__':

    facade = StartCarFacade()
    facade.start()


'''
Al crear la clase Facade hemos facilitado la realización del proceso al programa principal, 
liberándole de tener que acceder directamente a cada elemento del subsistema.
'''
