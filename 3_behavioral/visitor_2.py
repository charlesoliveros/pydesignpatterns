from __future__ import annotations
from abc import ABC, abstractmethod

'''
Veamos a continuación otro ejemplo algo más completo, en el que creamos un nueva clase 
de tipo IVisitor llamada EquipSpell que se encargará de conceder conjuros únicamente 
a los elementos de tipo Magician.
'''

class VisitorInterface(ABC):
    '''Visitor Interface'''

    @abstractmethod
    def visit(self, character: CharacterInterface)-> None:
        pass


class VisitorEquipWeapon(VisitorInterface):
    '''Visitor: visitor concreto'''
    
    def visit(self, character: CharacterInterface) -> None:
        if isinstance(character, Magician):
            character.weapon = 'Daga'
        elif isinstance(character, Warrior):
            character.weapon = 'Espada'
        elif isinstance(character, list):
            for a_character in characteres:
                a_character.accept(visitor=self)


class VisitorEquipSpell(VisitorInterface):
    '''Visitor: visitor concreto'''

    def visit(self, character: Magician) -> None:
        if isinstance(character, Magician):
            if character.magic_level <= 5:
                character.spell = 'Bola de Fuego'
            else:
                character.spell = 'Rayo de fuego'
        elif isinstance(character, list):
            for a_character in characteres:
                a_character.accept(visitor=self)


class CharacterInterface(ABC):
    '''Elemento interface'''

    @abstractmethod
    def accept(self, visitor: VisitorInterface) -> None:
        pass


class Warrior(CharacterInterface):
    '''Elemento concreto'''

    weapon: str

    def accept(self, visitor: VisitorInterface) -> None:
        visitor.visit(character=self)


class Magician(CharacterInterface):
    '''Elemento concreto'''

    weapon: str
    magic_level: int
    spell: str

    def __init__(self, magic_level: int) -> None:
        self.magic_level = magic_level

    def accept(self, visitor: VisitorInterface) -> None:
        visitor.visit(character=self)


if __name__ == '__main__':

    '''Crear los guerreros'''
    warrior1 = Warrior()
    warrior2 = Warrior()

    '''Crear los magos'''
    magician1 = Magician(magic_level=3)
    magician2 = Magician(magic_level=7)

    '''Crear una lista para guardar los elementos'''
    characteres = [warrior1, warrior2, magician1, magician2]

    '''Creamos el Visitor y le pasamos la lista para equiparlos con armas'''
    visitor_weapon = VisitorEquipWeapon()
    visitor_weapon.visit(character=characteres)

    '''Creamos el Visitor y le pasamos la lista para equiparlos con conjuros'''
    visitor_spell = VisitorEquipSpell()
    visitor_spell.visit(character=characteres)

    '''Comprobar el resultado:'''
    print(f'El arma del guerrero1 es {warrior1.weapon}')
    print(f'El arma del guerrero1 es {warrior1.weapon}')
    print(f'El arma del mago1 es {magician1.weapon}')
    print(f'El arma del mago1 es {magician2.weapon}')

    print(f'El conjuro del mago1 es {magician1.spell}')
    print(f'El cobjuro del mago1 es {magician2.spell}')


'''
Explicación:

- En este caso deseamos que los objetos Warrior puedan ser equipados con armas pero no 
con conjuros, los cuales sólo podrán equipados por objetos Magician.

- Para ello definimos una condición en el método accept() de la clase Warrior de modo que 
sólo admita objetos que sean específicamente de la clase VisitorEquipWeapon, llamando 
luego a su método visit().

- Por otro lado, el método visit() de la clase VisitorEquipSpell se utiliza para equipar 
con un conjuro diferente a los objetos de tipo Magician dependiendo de su nivel de magia.
'''
