from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List

'''
Veamos otro ejemplo de implementación de este patrón de diseño.
'''

class Mediator:
    '''Mediator'''

    button_hello: ButtonHello
    button_bye: ButtonBye
    button_delete: ButtonDelete
    text_box: TextBox

    def __init__(
        self,
        button_hello: ButtonHello,
        button_bye: ButtonBye,
        button_delete: ButtonDelete,
        text_box: TextBox,
    ) -> None:
        self.button_hello = button_hello
        self.button_bye = button_bye
        self.button_delete = button_delete
        self.text_box = text_box

    def click_hello(self) -> None:
        self.text_box.set_text('Hola!')

    def click_bye(self) -> None:
        self.text_box.set_text('Bye!')
    
    def click_delete(self) -> None:
        self.text_box.set_text()


class ComponentAbstract(ABC):
    '''ParnertAbstract en el ejemplo 1'''

    mediator: Mediator

    def set_mediator(self, mediator: Mediator) -> None:
        self.mediator = mediator


class ButtonHello(ComponentAbstract):

    def click_hello(self) -> None:
        self.mediator.click_hello()


class ButtonBye(ComponentAbstract):

    def click_bye(self) -> None:
        self.mediator.click_bye()


class ButtonDelete(ComponentAbstract):

    def click_delete(self) -> None:
        self.mediator.click_delete()


class TextBox(ComponentAbstract):

    def set_text(self, text: str = None) -> None:
        if text:
            print(text)
        else:
            print('El cuadro de texto esta vacio')


if __name__ == '__main__':
    '''Crear los objetos que participarán en la comunicación'''
    button_hello = ButtonHello()
    button_bye = ButtonBye()
    button_delete = ButtonDelete()
    text_box = TextBox()

    '''Crear el objeto mediador de la comunicación y agregarle los otros objetos'''
    mediator = Mediator(
        button_hello=button_hello,
        button_bye=button_bye,
        button_delete=button_delete,
        text_box=text_box
    )

    '''Definir en cada objeto el mediador usado en la comunicación'''
    button_hello.set_mediator(mediator)
    button_bye.set_mediator(mediator)
    button_delete.set_mediator(mediator)
    text_box.set_mediator(mediator)

    '''Provocar clics en los botones'''
    button_hello.click_hello()
    button_bye.click_bye()
    button_delete.click_delete()
    button_hello.click_hello()


'''
- En el programa principal creamos el objeto de tipo Mediator y los de tipo Component 
que participarán en la misma (observa que en este caso no es necesario crear la interface 
de Mediator ya que como se indica en el libro de GoF: 'no es necesario cuando los colegas 
sólo trabajan con un mediador').

- En dicha clase centralizadora se implementan diferentes métodos para procesar acciones 
dependiendo del componente que haya sido modificado en cada caso 
(el botón sobre el que se haga clic).

- A continuación se simulan clics en los diferentes botones, causando que varíe el contenido 
mostrado por el componente CuadroTexto.


Información:
El patrón de diseño Mediator es parecido al patrón Observer (en el que se define una 
dependencia de tipo 'uno a muchos' entre objetos de forma que cuando el que es observado 
cambia lo notifica al resto), sin embargo en el primero una clase central encapsula y 
dirige la comunicación generada entre los demás objetos.
'''
