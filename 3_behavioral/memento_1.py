'''
Este patrón de diseño es útil cuando manejamos un objeto que necesitaremos restaurar a 
estados anteriores (como por ejemplo cuando utilizamos la función de deshacer en un 
procesador de textos).

En la imagen de la derecha se muestra su estructura según el libro de GoF mencionado 
al principio del presente tutorial.
'''


class Memento:
    '''Memento'''

    name: str
    last_name: str

    def __init__(self, name: str, last_name: str) -> None:
        self.name = name
        self.last_name = last_name


class Originator:
    '''Originator'''

    name: str
    last_name: str

    def __init__(self, name: str, last_name: str) -> None:
        self.name = name
        self.last_name = last_name

    def create_memento(self) -> Memento:
        return Memento(self.name, self.last_name)

    def set_memento(self, memento: Memento) -> None:
        self.name = memento.name
        self.last_name = memento.last_name


class Caretaker:
    '''Caretaker
    sirve para guadar temporalmente el estado de un originator
    recibiendo el memento del metodo Originator.create_memento
    '''

    memento: Memento

    def __init__(self, memento: Memento = None) -> None:
        self.memento = memento


if __name__ == '__main__':
    '''Crear el objeto originador/creador'''
    originator = Originator(name='Carlos', last_name='Oliveros')

    '''Crear el objeto gestor Caretaker del Memento'''
    care_taker = Caretaker()

    '''Crear el Memento y asociarlo al objeto gestor'''
    care_taker.memento = originator.create_memento()

    '''Mostrar los datos del objeto'''
    print(f'Originador, nombre completo: {originator.name} {originator.last_name}')

    '''Modificar los datos del objeto originator'''
    print('Modificando datos de originator...')
    originator.name = 'Falcao'
    originator.last_name = 'Garcia'

    '''Mostrar los datos del objeto'''
    print(f'Originador, nombre completo: {originator.name} {originator.last_name}')

    '''Restaurar los datos del objeto'''
    print('Restaurando datos de originator')
    originator.set_memento(care_taker.memento)
    
    '''Mostrar los datos del objeto'''
    print(f'Originador, nombre completo: {originator.name} {originator.last_name}')


'''
- Al inicio del programa creamos un objeto de tipo Originator (que deseamos poder restaurar si 
se producen cambios) y otro de tipo Caretaker que utilizaremos a su vez para gestionar otro 
de tipo Memento (en el cual quedará registrado el estado anterior del primero de ellos, 
y que utilizaremos para restaurarlo).

- A continuación se muestran los datos iniciales del objeto de tipo Originator, y tras 
modificarlos se volverán a restaurar al estado inicial.

Basándonos en este ejemplo, en la clase Caretaker también podríamos haber definido una lista 
en la que ir guardando una instancia de Memento por cada modificación que se realizase en el 
objeto de tipo Originator, para de dicho modo implementar la funcionalidad de restaurar los 
últimos cambios de uno en uno.
'''
