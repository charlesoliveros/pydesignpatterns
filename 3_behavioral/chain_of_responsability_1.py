from __future__ import annotations
from abc import ABC, abstractmethod

'''
Este patrón puede resultarnos útil en casos en los que un objeto emisor de una petición 
desconozca qué objeto(s) podrá(n) atender a la misma.
'''


class HandlerAbstract(ABC):
    '''Manejador Abstracto'''

    next_handler: HandlerAbstract = None

    def get_next(self) -> HandlerAbstract:
        return self.next_handler
    
    def set_next(self, handler: HandlerAbstract) -> None:
        self.next_handler = handler
    
    @abstractmethod
    def check(self, status: str = None) -> None:
        pass


class HandlerAproved(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, status: str = None) -> None:
        if status is not None and status.lower() == 'aprobado':
            print('Solicitud Aprobada')
        elif self.next_handler is not None:
            '''Llamamos al método en el siguiente objeto'''
            print('Verificando proximo...')
            self.next_handler.check(status)


class HandlerDenied(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, status: str = None) -> None:
        if status is not None and status.lower() == 'denegado':
            print('Solicitud Denegada')
        elif self.next_handler is not None:
            '''Llamamos al método en el siguiente objeto'''
            print('Verificando proximo...')
            self.next_handler.check(status)


class HandlerPending(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, status: str = None) -> None:
        if status is None:
            print('Solicitud Pendiente')
        elif self.next_handler is not None:
            '''Llamamos al método en el siguiente objeto'''
            print('Verificando proximo...')
            self.next_handler.check(status)


if __name__ == '__main__':
    handler_aproved = HandlerAproved()
    handler_denied = HandlerDenied()
    handler_pending = HandlerPending()

    handler_aproved.set_next(handler_denied)
    handler_denied.set_next(handler_pending)

    print('--' * 20)
    handler_aproved.check('aprobado')
    print('--' * 20)
    handler_aproved.check('aprobado')
    print('--' * 20)
    handler_aproved.check('denegado')
    print('--' * 20)
    handler_aproved.check()
    print('--' * 20)
    handler_aproved.check('aprobado')


'''
- En la clase abstracta Handler definimos el método check() que deberán implementar las clases 
a partir de las cuales crearemos los objetos de dicho tipo.

- En dicho método se realizará la comprobación necesaria en cada caso para verificar si el 
objeto actual debe procesar la petición. En caso contrario, se pasará la misma al siguiente 
objeto de la cadena.

- En dicha clase definimos también los métodos 'set' y 'get' para establecer y obtener 
el siguiente objeto al que el actual deberá pasar la petición que ha recibido.

- En el programa principal se crean los objetos que formarán parte de la cadena de 
responsabilidad, utilizando el método set_next() para indicar a cada uno de ellos cuál es 
el siguiente en la misma.
'''
