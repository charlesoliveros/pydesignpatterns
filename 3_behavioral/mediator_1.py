from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List

'''
Una de las ventajas que ofrece la programación orientada a objetos (POO) es la posibilidad 
de reutilizar el código fuente, pero a medida que creamos objetos que se interrelacionan 
entre sí es menos probable que un objeto pueda funcionar sin la ayuda de otros.

Para evitar esto podemos utilizar el patrón Mediator, en el que se define una clase que 
hará de mediadora encapsulando la comunicación entre los objetos, evitándose con ello la 
necesidad de que lo hagan directamente entre sí.

Veámoslo mejor a continuación con un ejemplo sencillo.
'''

class MediatorInterface(ABC):
    '''Mediator Interface'''

    @abstractmethod
    def send(self, msg: str, partner: PartnerAbstract) -> None:
        pass


class Mediator(MediatorInterface):
    '''Mediator'''

    partners: List[PartnerAbstract]

    def __init__(self) -> None:
        self.partners = []

    def add_partner(self, partner: PartnerAbstract) -> None:
        self.partners.append(partner)

    def send(self, msg: str, partner_originator: PartnerAbstract) -> None:
        for partner in self.partners:
            if partner != partner_originator:
                partner.receive(msg)


class PartnerAbstract(ABC):

    mediator: MediatorInterface

    def __init__(self, mediator: MediatorInterface) -> None:
        self.mediator = mediator

    def comunicate(self, msg: str) -> None:
        self.mediator.send(msg=msg, partner_originator=self)

    @abstractmethod
    def receive(self, msg: str) -> None:
        '''Método a implementar por las clases que hereden'''
        pass


class Partner1(PartnerAbstract):

    def receive(self, msg: str) -> None:
        print(f'Partner1 received the msg: {msg}')


class Partner2(PartnerAbstract):

    def receive(self, msg: str) -> None:
        print(f'Partner2 received the msg: {msg}')


class Partner3(PartnerAbstract):

    def receive(self, msg: str) -> None:
        print(f'Partner3 received the msg: {msg}')


if __name__ == '__main__':
    '''Crear el objeto centralizador de la comunicación'''
    mediator = Mediator()

    '''Crear los objetos que participarán en la comunicación'''
    partner1 = Partner1(mediator)
    partner2 = Partner2(mediator)
    partner3 = Partner3(mediator)

    '''Agregarlos al objeto centralizador'''
    mediator.add_partner(partner1)
    mediator.add_partner(partner2)
    mediator.add_partner(partner3)

    '''Provocar un cambio en un uno de los elementos'''
    partner2.comunicate('Partner2 dice hola!')


'''
- Al inicio del programa creamos el objeto de tipo Mediator encargado de dirigir la 
comunicación, y los de tipo Partner que participarán en la misma.

- A continuación se provoca un cambio en uno de los objetos de tipo Partner a través del 
método comunicate() (implementado en la clase abstracta de la que heredan).

- En dicho método se envían los datos (junto con una referencia al objeto modificado) 
al objeto de tipo Mediator para que a su vez los reenvíe a los demás objetos que participan 
en la comunicación (observa que no los envía también al objeto que sufrió el cambio).
'''
