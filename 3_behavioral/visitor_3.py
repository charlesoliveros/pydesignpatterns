from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List

'''
En los ejemplos anteriores hemos omitido la creación de una clase de tipo ObjetoEstructura 
para facilitar la comprensión de cómo funciona este patrón de diseño. Veamos ahora un ejemplo 
en el que sí la implementamos.

En el siguiente programa tenemos una clase InvoiceGroup (de tipo ObjetoEstructura) en el 
que se guardan referencias a objetos Invoice (que a su vez contienen otros de tipo Article). 
Al ejecutarlo se mostrarán las facturas que se hayan agregado junto con los artículos que 
contienen.
'''

class ElementInterface(ABC):
    '''Elemento interface'''

    @abstractmethod
    def accept(self, visitor: VisitorInterface) -> None:
        pass


class Article(ElementInterface):
    '''Elemento concreto'''

    name: str
    quantity: int

    def __init__(self, name: str, quantity: int) -> None:
        self.name = name
        self.quantity = quantity

    def accept(self, visitor: VisitorInterface) -> None:
        visitor.visit(element=self)


class Invoice(ElementInterface):
    '''Elemento concreto'''
    
    code: str
    articles: List[Article]

    def __init__(self, code: str) -> None:
        self.code = code
        self.articles = []

    def add_article(self, article: Article) -> None:
        self.articles.append(article)

    def accept(self, visitor: VisitorInterface) -> None:
        '''Procesar el visitor para la factura'''
        visitor.visit(element=self)

        '''Procesar el visitor para cada artículo en la factura'''
        for article in self.articles:
            article.accept(visitor=visitor)


class InvoiceGroup(ElementInterface):
    '''Objeto Estructura'''

    invoices: List[Invoice]

    def __init__(self) -> None:
        self.invoices = []

    def add_invoice(self, invoice: Invoice) -> None:
        self.invoices.append(invoice)

    def get_invoice(self, position: int) -> Invoice:
        return self.invoices[position]
    
    def accept(self, visitor: VisitorInterface) -> None:
        for invoice in self.invoices:
            invoice.accept(visitor)


class VisitorInterface(ABC):
    '''Visitor Interface'''

    @abstractmethod
    def visit(self, element: ElementInterface)-> None:
        pass


class Show(VisitorInterface):
    '''Visitor Concreto'''

    def visit(self, element: ElementInterface) -> None:
        if isinstance(element, Invoice):
            print('--'*20)
            print(f'Factura numero {element.code}')
        elif isinstance(element, Article):
            print(f'Articulo {element.name}: {element.quantity} unidades')


if __name__ == '__main__':
    '''Cliente'''

    '''Creamos el objeto estructura'''
    invoice_group = InvoiceGroup()

    '''Creamos una factura y le pasamos los elementos'''
    invoce1 = Invoice(code='001')
    invoce1.add_article(Article(name='Tornillos', quantity=40))
    invoce1.add_article(Article(name='Tuercas', quantity=80))
    invoce1.add_article(Article(name='Taladros', quantity=65))

    '''Creamos otra factura y le pasamos los elementos'''
    invoce2 = Invoice(code='002')
    invoce2.add_article(Article(name='Martillos', quantity=30))
    invoce2.add_article(Article(name='Linternas', quantity=90))

    '''Agregamos las facturas al objeto estructura'''
    invoice_group.add_invoice(invoce1)
    invoice_group.add_invoice(invoce2)

    '''Pasamos el visitor al objeto de estructura para que recorra todas las facturas y 
    los artículos que contiene'''
    invoice_group.accept(Show())


'''
Explicación:

- Al principio del programa creamos dos objetos de tipo Invoice agregándoles los de tipo Article 
(de tipo ElementInterface ambos).

- A continuación creamos un objeto InvoiceGroup (de tipo ObjetoEstructura) y le agregamos cada 
objeto de tipo Invoice creado.

- Observa que la clase InvoiceGroup implementa la interface ElementInterface, al igual que 
Invoice y Article.

- Después se pasa el objeto de tipo Show como parámetro al método accept() de InvoiceGroup, 
en el cual se recorrerán todas las facturas para enviar dicho objeto a cada una de ellas.

. En el método accept() de cada Invoice se llama a su vez al método visit() de Show 
(el VisitorConcreto) enviándole como parámetro el propio objeto Invoice, y posteriormente 
cada uno de los objeto Article que contiene.

- En dicho método se mostrará el código de la factura por un lado, y la información de los 
artículos (nombre y unidades) por otro.
'''
