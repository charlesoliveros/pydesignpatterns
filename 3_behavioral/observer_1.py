from abc import ABC, abstractmethod
from typing import List

'''
El patrón Observer puede ser utilizado cuando hay objetos que dependen de otro, 
necesitando ser notificados en caso de que se produzca algún cambio en él.

Se utilizarán los métodos add_watcher() y remove_watcher() de la clase abstracta AWatched 
para registrar en una lista qué objetos de tipo AWatched deberán ser notificados o dejar 
de serlo cuando se produzca algún cambio en él (en tal caso recorrerá dicha lista para enviar 
una notificación a cada uno de ellos).

El mensaje será enviado a Watched (que implementa la interface WatcherInterface) utilizando 
su método watched_updated().

Veamos a continuación un ejemplo sencillo en el cual cuando se produce un cambio en el 
observado éste envía una notificación a los observadores, que simplemente mostrarán un 
mensaje al recibirla.
'''

class WatcherInterface(ABC):
    '''Watcher Interface: Observador interface'''

    @abstractmethod
    def watched_updated(self) -> None:
        pass


class Watcher(WatcherInterface):
    '''Watcher: Observador concreto'''

    name: str

    def __init__(self, name: str) -> None:
        self.name = name
        print(f'Observador {self.name} creado')
    
    def watched_updated(self) -> None:
        print(f'Observador {self.name} recibe la notificación')


class WatchedAbstract(ABC):
    '''Watched Abstract: Observado abstracto'''

    watchers: List[WatcherInterface]

    def __init__(self) -> None:
        self.watchers = []

    def add_watcher(self, watcher: WatcherInterface) -> None:
        self.watchers.append(watcher)
        '''Según nuestro ejemplo, cada vez que se agregue un nuevo observador, 
        los existentes serán notificados
        '''
        self.notity_watchers()
    
    def remove_watcher(self, watcher: WatcherInterface) -> None:
        self.watchers.remove(watcher)
    
    def notity_watchers(self) -> None:
        '''Enviar la notificación a cada observador a través de su propio método'''
        for watcher in self.watchers:
            watcher.watched_updated()


class Watched(WatchedAbstract):

    pass


if __name__ == '__main__':
    '''Instanciar el objeto que será Observado'''
    watched = Watched()

    '''Instanciar y registrar un Observador'''
    watcher1 = Watcher(name='Marcela')
    watched.add_watcher(watcher1)
    
    '''Instanciar y registrar un Observador'''
    watcher2 = Watcher(name='Sebas')
    watched.add_watcher(watcher2)

    '''Instanciar y registrar un Observador'''
    watcher3 = Watcher(name='Laura')
    watched.add_watcher(watcher3)


'''
- Al inicio del programa se crea el objeto que será observado así como tres objetos 
observadores del anterior, que se agregan a la lista del observado.

- Cuando se produce un cambio en el observado (cuando se le agrega un nuevo observador) 
éste envía una notificación a los observadores (sin pasar ningún tipo de parámetro en 
este ejemplo).
'''
