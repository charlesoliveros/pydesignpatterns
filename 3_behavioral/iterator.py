from __future__ import annotations
from abc import ABC, abstractmethod

'''
Según el libro de GoF, podemos utilizarlo cuando necesitemos recorrer secuencialmente 
los objetos de un elemento agregado sin exponer su representación interna.

Así pues, este patrón de diseño nos resultará útil para acceder a los elementos de un array 
o colección de objetos contenida en otro objeto.

A continuación mostramos un ejemplo de uso.
'''


class IteratorInterface(ABC):
    '''Iterator'''

    @abstractmethod
    def first(self) -> object:
        pass
    
    @abstractmethod
    def next(self) -> object:
        pass
    
    @abstractmethod
    def exists_more(self) -> bool:
        pass
    
    @abstractmethod
    def current(self) -> object:
        pass


class AggregateInterface(ABC):
    '''Agregado'''

    @abstractmethod
    def get_iterator(self) -> IteratorInterface:
        pass


class Aggregate(AggregateInterface):
    '''Agregado Concreto'''

    _data: list

    def __init__(self) -> None:
        self._data = []
        self.fill()

    def get_iterator(self) -> IteratorInterface:
        return Iterator(self)

    def fill(self) -> None:
        self._data.append('Carlos')
        self._data.append('Laura')
        self._data.append('Sebastian')
        self._data.append('Marcela')


class Iterator(IteratorInterface):
    '''Iterator Concreto'''

    aggregate: Aggregate
    current_position: int = 0

    def __init__(self, aggregate: Aggregate) -> None:
        self.aggregate = aggregate
    
    def first(self) -> object:
        obj = None

        if self.aggregate._data:
            self.current_position = 0
            obj = self.aggregate._data[self.current_position]

        return obj

    def next(self) -> object:
        obj = None
        if self.exists_more():
            obj = self.aggregate._data[self.current_position]
            self.current_position += 1
        
        return obj

    def current(self) -> object:
        obj = None
        if self.exists_more():
            obj = self.aggregate._data[self.current_position]
        
        return obj

    def exists_more(self) -> bool:
        if self.current_position < len(self.aggregate._data):
            return True
        return False


if __name__ == '__main__':

    '''Crear el objeto agregado que contiene la lista (un vector en este ejemplo)'''
    aggregate = Aggregate()

    '''Crear el objeto iterador para recorrer la lista'''
    iterator = aggregate.get_iterator()

    '''Mover una posición adelante y mostrar el elemento'''
    print(iterator.first())

    '''Mover dos posiciones adelante'''
    iterator.next()
    iterator.next()

    '''Mostrar el elemento actual'''
    print(iterator.current())

    '''Volver al principio'''
    iterator.first()

    '''Recorrer todo'''
    print('--' * 20)
    print('Recorrer todo...')
    while iterator.exists_more():
        print(iterator.next())
