from __future__ import annotations
from abc import ABC, abstractmethod

'''
En este otro ejemplo realizamos una modificación en el anterior de modo que cada objeto 
de tipo TrafficLightStateAbstract contenga una referencia al objeto que lo utiliza:
'''

class TrafficLightStateAbstract(ABC):
    '''State Abstract: estado abstracto'''

    traffic_light:TrafficLight

    def __init__(self, traffic_light: TrafficLight) -> None:
        '''Agregamos una referencia al semaforo al momento de instanciar in estado'''
        self.traffic_light = traffic_light

    @abstractmethod
    def show(self) -> None:
        '''Método que deberán crear las clases que hereden de ésta'''
        pass


class TrafficLightStateGreen(TrafficLightStateAbstract):
    '''State: Estado concreto'''

    def show(self) -> None:
        print(f'Semaforo {self.traffic_light.code} esta en: Luz verde')


class TrafficLightStateYellow(TrafficLightStateAbstract):
    '''State: Estado concreto'''

    def show(self) -> None:
        print(f'Semaforo {self.traffic_light.code} esta en: Luz amarilla')


class TrafficLightStateRed(TrafficLightStateAbstract):
    '''State: Estado concreto'''

    def show(self) -> None:
        print(f'Semaforo {self.traffic_light.code} esta en: Luz roja')


class TrafficLight:
    '''Context: contexto'''

    traffic_light_state: TrafficLightStateAbstract
    code: str

    def __init__(self, code: str) -> None:
        self.traffic_light_state = TrafficLightStateGreen(traffic_light=self)
        self.code = code

    def set_state(self, traffic_light_state: TrafficLightStateAbstract) -> None:
        self.traffic_light_state = traffic_light_state
    
    def show(self) -> None:
        self.traffic_light_state.show()


if __name__ == '__main__':

    traffic_light = TrafficLight(code='001')
    '''Muestra el aviso por defecto (verde, no hay alerta)'''
    traffic_light.show()

    traffic_light.set_state(TrafficLightStateYellow(traffic_light))
    traffic_light.show()

    traffic_light.set_state(TrafficLightStateRed(traffic_light))
    traffic_light.show()


'''
Explicación:

- En este caso a la hora de cambiar el estado de un objeto de tipo Semaforo, a la instancia 
de estado que le pasamos como parámetro le agregamos una referencia a dicho objeto, quedando 
registrada en la propiedad traffic_light de ésta, de forma que en caso necesario pueda acceder 
a su contenedor.

Información
Este patrón de diseño es parecido al patrón Strategy, en el que se crea un objeto indicando 
el comportamiento que deberá tener, pero sin la posibilidad de ser modificado posteriormente.
'''
