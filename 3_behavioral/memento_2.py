from typing import List

'''
Ahora veamos un último ejemplo en que utilizamos este patrón para guardar y recuperar 
marcadores a las páginas de un libro:
'''

class Memento:

    status: int

    def __init__(self, status: int) -> None:
        self.status = status


class MarkManager:
    '''Caretaker del ejemplo 1'''

    markers: List[Memento]

    def __init__(self) -> None:
        self.markers = []

    def add_marker(self, memento: Memento) -> None:
        self.markers.append(memento)

    def get_marker(self, position: int) -> Memento:
        return self.markers[position]


class Book:
    '''Originator del ejemplo 1'''

    current_page: int

    def go_page(self, page: int) -> None:
        print(f'Ir a página {page}...')
        self.current_page = page
        print(f'Página actual: {self.current_page}')

    def create_marker(self) -> Memento:
        print(f'Marcador creado en pagina {self.current_page}')
        return Memento(status=self.current_page)

    def go_back_page(self, memento: Memento) -> None:
        self.current_page = memento.status
        print(f'Volvemos a la pagina {self.current_page}')


if __name__ == '__main__':

    mark_manager = MarkManager()

    '''Abrimos el libro y vamos a la página 10'''
    book = Book()
    book.go_page(10)

    '''Guardamos la página en marcadores'''
    mark_manager.add_marker(book.create_marker())
    
    '''Saltamos a la página 83'''
    book.go_page(83)

    '''Guardamos la página en marcadores: se guarda un memento obj'''
    mark_manager.add_marker(book.create_marker())

    '''Volvemos a la primera página guardada en marcadores'''
    book.go_back_page(mark_manager.get_marker(0))

    '''Saltamos a la segunda página guardada en marcadores'''
    book.go_back_page(mark_manager.get_marker(1))


'''
- Al iniciarse el programa se crea tanto una instancia de la clase Book como otra de 
MarkManager, que guardará una lista en la que se almacenarán las referencias a los marcadores.

- Cada marcador es creado utilizando el método create_marker() de un objeto Libro, quedando 
almacenado el número de página en un objeto de tipo Memento, el cual será posteriormente 
guardado en una lista mediante el método add_marker() de MarkManager.

- El uso del método go_page() es méramente ilustrativo, no siendo necesario para el 
correcto funcionamiento del ejemplo.
'''
