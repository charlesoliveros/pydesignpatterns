from __future__ import annotations
from abc import ABC, abstractmethod
from typing import Any

'''
Según el libro de GoF este patrón permite añadir funcionalidades a una clase sin tener 
que modificarla, siendo usado para manejar algoritmos, relaciones y responsabilidades 
entre objetos.

Así pues, nos resultará útil cuando necesitemos realizar operaciones distintas y no 
relacionadas sobre una estructura de objetos, aunque si lo utilizamos y luego tenemos 
que modificar alguna de las clases implicadas, hemos de tener en cuenta que se produce 
cierto nivel de acoplamiento entre ellas.

Veamos un ejemplo en el que simulando un juego equipamos con un arma a personajes de tipo 
Guerrero (Warrior) y con otra diferente a los que son de tipo Mago (magician).
'''

class VisitorInterface(ABC):
    '''Visitor Interface'''

    @abstractmethod
    def visit(self, character: CharacterInterface)-> None:
        pass


class VisitorEquipWeapon(VisitorInterface):
    '''Visitor: visitor concreto'''
    
    def visit(self, character: CharacterInterface) -> None:
        if isinstance(character, Magician):
            character.weapon = 'Daga'
        elif isinstance(character, Warrior):
            character.weapon = 'Espada'
        elif isinstance(character, list):
            for a_character in characteres:
                a_character.accept(visitor=self)
        '''Si necesito agregar o modificar alguna caracteristica, lo hago aqui y no
        tengo que mofificar las clases concretas
        '''


class CharacterInterface(ABC):
    '''Elemento interface'''

    @abstractmethod
    def accept(self, visitor: VisitorInterface) -> None:
        pass


class Warrior(CharacterInterface):
    '''Elemento concreto'''

    weapon: str

    def accept(self, visitor: VisitorInterface) -> None:
        visitor.visit(character=self)


class Magician(CharacterInterface):
    '''Elemento concreto'''

    weapon: str

    def accept(self, visitor: VisitorInterface) -> None:
        visitor.visit(character=self)


if __name__ == '__main__':

    '''Crear los elementos'''
    warrior1 = Warrior()
    warrior2 = Warrior()

    magician1 = Magician()
    magician2 = Magician()

    '''Crear una lista para guardar los elementos'''
    characteres = [warrior1, warrior2, magician1, magician2]

    '''Creamos el Visitor y le pasamos la lista'''
    visitor = VisitorEquipWeapon()
    visitor.visit(character=characteres)

    '''Comprobar el resultado:'''
    print(f'El arma del guerrero1 es {warrior1.weapon}')
    print(f'El arma del guerrero1 es {warrior1.weapon}')
    print(f'El arma del mago1 es {magician1.weapon}')
    print(f'El arma del mago1 es {magician2.weapon}')


'''
Explicación:

- Al inicio del programa creamos una lista y la rellenamos con objetos de tipo CharacterInterface.

- A continuación pasamos dicha lista al método visit() del objeto de tipo VisitorEquipWeapon, 
en el cual la recorreremos llamando al método accept() de cada objeto de tipo CharacterInterface 
encontrado.

- Observa que el método accept() en los objetos de tipo CharacterInterface no hace otra cosa 
que redirigir hacia el método visit() de los objetos de tipo VisitorInterface, en el cual se 
realizan las operaciones necesarias sobre el elemento del que se trate en cada caso.
'''
