from abc import ABC, abstractmethod

'''
Este sencillo patrón resulta útil en casos en los que podamos implementar en una clase 
abstracta el código común que será usado por las clases que heredan de ella, 
permitiéndoles que implementen el comportamiento que varía mediante la reescritura 
(total o parcial) de determinados métodos.

La diferencia con la forma común herencia y sobreescritura de los métodos abstractos 
estriba en que la clase abstracta contiene un método denominado 'plantilla' que hace 
llamadas a los que han de ser implementados por las clases que hereden de ella.

A continuación veremos un ejemplo en el que utilizamos este patrón de diseño.
'''

class MyClassAbstract(ABC):
    '''Clase Abstracta'''

    def get_result(self, num: int) -> int:
        '''Metodo Plantilla'''
        self.message()
        num_res = self.multiply(num)
        num_res = self.sum(num_res)
        return num_res
    
    def message(self) -> None:
        print('Vamos a realizar las operaciones...')

    @abstractmethod
    def multiply(self) -> int:
        '''Método que deberá implementar las subclases'''
        pass
    
    @abstractmethod
    def sum(self) -> int:
        '''Método que deberá implementar las subclases'''
        pass


class MyClass1(MyClassAbstract):
    '''Clase concteta'''

    def multiply(self, num: int) -> int:
        return num * 100
    
    def sum(self, num: int) -> int:
        return num + 10


class MyClass2(MyClassAbstract):
    '''Clase concteta'''

    def multiply(self, num: int) -> int:
        return num * 1000
    
    def sum(self, num: int) -> int:
        return num + 100


if __name__ == '__main__':
    obj1 = MyClass1()
    obj2 = MyClass2()

    '''Obtener el resultado de multiplicar 3 por 100 y restarle 10'''
    num1 = obj1.get_result(3)
    print(f'num1 = {num1}')

    '''Obtener el resultado de multiplicar 3 por 1000 y restarle 100'''
    num2 = obj2.get_result(3)
    print(f'num2 = {num2}')


'''
- En MyClassAbstract se define el método plantilla get_result(), desde el que se llama 
a dos métodos definidos como abstractos (que serán implementados por las subclases).

- En MyClass1 se implementan dichos métodos de forma que uno de ellos devuelve el número entero 
recibido como parámetro multiplicado por 100, y otro agregándole 10.

- En MyClass2 se implementan de modo que lo devuelven multiplicado por 1000 y 
agregándole 100, respectivamente.

En ciertos casos será necesario que algunas subclases sobreescriban determinados métodos 
de la clase padre para agregar determinadas funcionalidades.

Por ejemplo, si queremos que la nueva clase MyClass3 sobreescriba el método message() con 
el fin de que se muestre otro texto antes del ya definido en dicho método lo haríamos 
del siguiente modo:
'''


class MyClass3(MyClassAbstract):
    '''Clase concreta'''

    def message(self) -> None:
        print('En clase clase concreta MyClass3')
        super().message()

    def multiply(self, num: int) -> int:
        return num * 1000
    
    def sum(self, num: int) -> int:
        return num + 100


if __name__ == '__main__':
    obj3 = MyClass3()

    num3 = obj3.get_result(3)
    print(f'num3 = {num1}')
