from abc import ABC ,abstractmethod

'''
Podemos hacer uso de este patrón para crear un objeto que pueda comportarse de formas 
diferentes (lo cual se definirá en el momento de su instanciación o creación).

Para comprender mejor cómo funciona veamos un sencillo ejemplo:
'''

class ArithmeticInterface(ABC):
    '''Strategy Interface'''

    @abstractmethod
    def execute_operation(self, a: int, b: int) -> int:
        pass


class Sum(ArithmeticInterface):
    '''Strategy: Estrategia Concreta'''

    def execute_operation(self, a: int, b: int) -> int:
        return a + b


class Subtract(ArithmeticInterface):
    '''Strategy: Estrategia Concreta'''

    def execute_operation(self, a: int, b: int) -> int:
        return a - b


class Multiply(ArithmeticInterface):
    '''Strategy: Estrategia Concreta'''

    def execute_operation(self, a: int, b: int) -> int:
        return a * b


class  ArithmeticOperation:
    '''Context: Contexto'''

    strategy: ArithmeticInterface

    def __init__(self, strategy: ArithmeticInterface) -> None:
        self.strategy = strategy
    
    def process(self, a: int, b: int) -> int:
        return self.strategy.execute_operation(a, b)


if __name__ == '__main__':
    a = 3
    b = 4

    context = ArithmeticOperation(Sum())
    result_sum = context.process(a, b)

    context = ArithmeticOperation(Subtract())
    result_sub = context.process(a, b)

    context = ArithmeticOperation(Multiply())
    result_mul = context.process(a, b)

    print(f'Suma: {result_sum}')
    print(f'Resta: {result_sub}')
    print(f'Multiplicación: {result_mul}')


'''
- Se crea la interface IAritmetica, en la que definimos el método que deberán contener 
las clases que la implementen.

- La clase OperacionAritmetica contiene una referencia a un objeto de tipo IAritmetica, 
para así poder utilizarlo cuando sea necesario.

- Al inicio del programa creamos tres instancias de OperacionAritmetica, indicando 
el comportamiento que deberá tener cada una de ellas.

- A continuación se realizan las operaciones y se muestra el resultado.

Información:
Este patrón de diseño es parecido al patrón State (en el que un objeto puede cambiar 
su estado interno durante la ejecución del programa pudiendo comportarse de forma diferente 
entonces), mientras que con Strategy se crean varios objetos de un mismo tipo pero 
pudiendo tener distintos comportamientos cada uno de ellos.
'''
