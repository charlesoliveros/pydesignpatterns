from __future__ import annotations
from abc import ABC, abstractmethod
from typing import List, Any

'''
En este último ejemplo, cuando se produce un cambio en el observado éste envía una 
notificación a los observadores (que en este caso al ser instanciados guardan una 
referencia a él) los cuales se encargarán de consultarle directamente para averiguar 
la modificación producida.
'''

class WatcherInterface(ABC):
    '''Watcher Interface: Observador interface'''

    @abstractmethod
    def watched_updated(self) -> None:
        pass


class Watcher(WatcherInterface):
    '''Watcher: Observador concreto'''

    name: str
    watched: Watched

    def __init__(self, name: str, watched: Watched) -> None:
        self.name = name
        self.watched = watched
        watched.add_watcher(self)
    
    def watched_updated(self) -> None:
        print(
            f'Observador {self.name} recibió la notificación: '
            f'El nombre del objeto "observado" cambío a {self.watched.name}'
            )


class WatchedAbstract(ABC):
    '''Watched Abstract: Observado abstracto'''

    name: str = None
    watchers: List[WatcherInterface]

    def __init__(self, name: str) -> None:
        self.name = name
        self.watchers = []
        print(f'Objeto Observado {self.name} creado')

    def set_name(self, name: str) -> None:
        self.name = name
        self.notity_watchers()

    def add_watcher(self, watcher: WatcherInterface) -> None:
        self.watchers.append(watcher)
    
    def remove_watcher(self, watcher: WatcherInterface) -> None:
        self.watchers.remove(watcher)
    
    def notity_watchers(self) -> None:
        '''Enviar la notificación a cada observador a través de su propio método'''
        for watcher in self.watchers:
            watcher.watched_updated()


class Watched(WatchedAbstract):

    pass


if __name__ == '__main__':
    '''Instanciar el objeto que será Observado'''
    watched = Watched(name='Jose')

    '''Instanciar y registrar un Observador'''
    watcher1 = Watcher(name='Marcela', watched=watched)
    
    '''Instanciar y registrar un Observador'''
    watcher2 = Watcher(name='Sebas', watched=watched)

    '''Instanciar y registrar un Observador'''
    watcher3 = Watcher(name='Laura', watched=watched)

    watched.set_name('Pepe')
    watched.set_name('Ronaldo')


'''
- Se crea el objeto que será observado junto con tres observadores del anterior, a los que 
les pasa como parámetro una referencia de dicho objeto Observado.

- Se modifica un valor del objeto observado.

- Cuando se produce un cambio en el objeto observado (se asigna un nuevo valor al atributo 
numero), en el mismo método set_name() se llama al método notity_watchers() implementado 
en la clase abstracta de la que hereda (la cual a su vez llama al método watched_updated() 
que implementan los objetos observadores, en el que cada uno de ellos se encargará de 
consultar al observado para comprobar los cambios producidos).
'''
