from __future__ import annotations
from abc import ABC, abstractmethod

'''
En el ejemplo anterior, cuando un objeto procesa la petición ésta no se continúa pasando 
al resto de ellos. Para implementar dicha funcionalidad bastaría con quitar el 'elif' en el 
método comprobar() de cada clase.

Veamos un ejemplo de cómo hacerlo; comprobar si un numero es negativo, positvo o 
esta dentro de un rango:
'''


class HandlerAbstract(ABC):
    '''Manejador Abstracto'''

    next_handler: HandlerAbstract = None

    def get_next(self) -> HandlerAbstract:
        return self.next_handler
    
    def set_next(self, handler: HandlerAbstract) -> None:
        self.next_handler = handler
    
    @abstractmethod
    def check(self, number: int = None) -> None:
        pass


class HandlerPositive(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, number: int = None) -> None:
        if number > 0:
            print('El número es positivo')

        if self.next_handler is not None:
            print('Verificando proximo...')
            self.next_handler.check(number) 


class HandlerNegative(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, number: int = None) -> None:
        if number < 0:
            print('El número es negativo')

        if self.next_handler is not None:
            print('Verificando proximo...')
            self.next_handler.check(number) 


class HandlerPositiveRange(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, number: int = None) -> None:
        if number in range(1, 51):
            print('El número esta entre 1 y 50')

        if self.next_handler is not None:
            print('Verificando proximo...')
            self.next_handler.check(number) 


if __name__ == '__main__':
    handler_pos = HandlerPositive()
    handler_neg = HandlerNegative()
    handler_pos_ran = HandlerPositiveRange()

    handler_pos.set_next(handler_neg)
    handler_neg.set_next(handler_pos_ran)

    print('--' * 20)
    handler_pos.check(60)
    print('--' * 20)
    handler_pos.check(-30)
    print('--' * 20)
    handler_pos.check(40)
