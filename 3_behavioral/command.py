from abc import ABC, abstractmethod
from typing import List
'''
Este patrón resulta útil en escenarios en los que se han de enviar peticiones a otros 
objetos sin saber qué operación se ha de realizar, y ni tan siquiera quién es el 
receptor de dicha petición.

Dicho de otro modo: tenemos varios objetos que realizan acciones similares de forma 
diferente, y queremos que se procese la adecuada dependiendo del objeto solicitado.

El Invocador no sabe quién es el Receptor ni la acción que se realizará, tan sólo 
invoca un Comando que ejecuta la acción adecuada:

A continuación mostramos un ejemplo muy básico de uso este patrón, en el que creamos 
un menú de opciones.
'''


class MenuItemInterface(ABC):
    '''Command'''

    @abstractmethod
    def execute(self) -> None:
        pass


class Receptor:
    '''Receptor'''

    def action(self, action: str) -> None:
        action = action.lower()
        if action == 'abrir':
            print('Abrir Documento')
        elif action == 'imprimir':
            print('Imprimir Documento')
        elif action == 'salir':
            print('Salir del programa')
        else:
            print('Opcion no valida')


class Menu:
    '''Invocador'''

    _menu: List[MenuItemInterface] = [] 

    def add_item(self, menu_item: MenuItemInterface) -> None:
        self._menu.append(menu_item)

    def get_item(self, menu_item_pos: int) -> MenuItemInterface:
        return self._menu[menu_item_pos]


class MenuItemOpen(MenuItemInterface):
    '''Concrete Command'''

    receptor: Receptor

    def __init__(self, receptor: Receptor) -> None:
        self.receptor = receptor

    def execute(self) -> None:
        self.receptor.action('abrir')


class MenuItemPrint(MenuItemInterface):
    '''Concrete Command'''

    receptor: Receptor

    def __init__(self, receptor: Receptor) -> None:
        self.receptor = receptor

    def execute(self) -> None:
        self.receptor.action('imprimir')


class MenuItemExit(MenuItemInterface):
    '''Concrete Command'''

    receptor: Receptor

    def __init__(self, receptor: Receptor) -> None:
        self.receptor = receptor

    def execute(self) -> None:
        self.receptor.action('exit')


if __name__ == '__main__':

    '''Crear el objeto Menú (el Invocador)'''
    menu = Menu()

    '''Crear el Receptor'''
    receptor = Receptor()

    '''Crear las opciones de menú, indicándoles el Receptor'''
    menu_item_open = MenuItemOpen(receptor)
    menu_item_print = MenuItemPrint(receptor)
    menu_item_exit = MenuItemExit(receptor)

    '''Agregar las opciones al Menú'''
    menu.add_item(menu_item_open)
    menu.add_item(menu_item_print)
    menu.add_item(menu_item_exit)

    '''Ejecutar cada opción del menú'''
    menu.get_item(0).execute()
    menu.get_item(1).execute()
    menu.get_item(2).execute()


'''
- Al inicio del programa creamos una instancia de la clase Menu (Invocador en el diagrama) 
junto con otra de Receptor, y después tres elementos de menú (que implementan IMenuItem) 
a los cuales pasamos la referencia del Receptor.

- A continuación agregamos las opciones al Menu (en el cual quedarán registradas para su 
posterior acceso).

- Finalmente ejecutamos cada una de las opciones del menú.
'''
