from abc import ABC, abstractmethod
from typing import List, Any

'''
En este otro ejemplo mostramos una forma más recomendable de utilizar este patrón.
'''

class WatcherInterface(ABC):
    '''Watcher Interface: Observador interface'''

    @abstractmethod
    def watched_updated(self, value: Any) -> None:
        pass


class Watcher(WatcherInterface):
    '''Watcher: Observador concreto'''

    name: str

    def __init__(self, name: str) -> None:
        self.name = name
        print(f'Observador {self.name} creado')
    
    def watched_updated(self, value: Any) -> None:
        print(
            f'Observador {self.name} recibió la notificación: '
            f'El nombre del objeto "observado" cambío a {value}'
            )


class WatchedAbstract(ABC):
    '''Watched Abstract: Observado abstracto'''

    name: str = None
    watchers: List[WatcherInterface]

    def __init__(self, name: str) -> None:
        self.name = name
        self.watchers = []
        print(f'Objeto Observado {self.name} creado')

    def set_name(self, name: str) -> None:
        self.name = name
        self.notity_watchers(value=name)

    def add_watcher(self, watcher: WatcherInterface) -> None:
        self.watchers.append(watcher)
    
    def remove_watcher(self, watcher: WatcherInterface) -> None:
        self.watchers.remove(watcher)
    
    def notity_watchers(self, value: Any) -> None:
        '''Enviar la notificación a cada observador a través de su propio método'''
        for watcher in self.watchers:
            watcher.watched_updated(value=value)


class Watched(WatchedAbstract):

    pass


if __name__ == '__main__':
    '''Instanciar el objeto que será Observado'''
    watched = Watched(name='Jose')

    '''Instanciar y registrar un Observador'''
    watcher1 = Watcher(name='Marcela')
    watched.add_watcher(watcher1)
    
    '''Instanciar y registrar un Observador'''
    watcher2 = Watcher(name='Sebas')
    watched.add_watcher(watcher2)

    '''Instanciar y registrar un Observador'''
    watcher3 = Watcher(name='Laura')
    watched.add_watcher(watcher3)

    watched.set_name('Pepe')
    watched.set_name('Ronaldo')


'''
- Al inicio del programa se crea el objeto que será observado así como tres 
observadores que se agregan a su lista.

- Se modifica por dos veces un valor del objeto observado.

- Cuando se produce un cambio en el observado (se asigna un nuevo valor al atributo nombre), 
en el mismo método set_name() se llama al método notity_watchers() (que a su vez llama al 
método watched_updated() que implementan los objetos observadores) para enviarles una 
notificación, junto con el valor modificado.
'''
