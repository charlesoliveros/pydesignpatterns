from __future__ import annotations
from abc import ABC, abstractmethod

'''
Otra forma de implementar este patrón es creando un método en la clase abstracta que será 
utilizado para enviar la petición (en lugar de que lo haga cada ManejadorConcreto, como en 
el ejemplo anterior).

Veamos a continuación un ejemplo de cómo hacerlo.
'''


class HandlerAbstract(ABC):
    '''Manejador Abstracto'''

    next_handler: HandlerAbstract = None

    def get_next(self) -> HandlerAbstract:
        return self.next_handler
    
    def set_next(self, handler: HandlerAbstract) -> None:
        self.next_handler = handler
    
    def process(self, number: int) -> None:
        '''Este es el metodo que envía la peticiín al siguiente objeto en la cadena,
        evitando asi que se tenga que enviar desde las clases concretas
        '''
        if number in range(-100, 101):
            self.check(number)
        
        if self.next_handler is not None:
            print('Verificando proximo...')
            self.next_handler.process(number)
    
    @abstractmethod
    def check(self, number: int = None) -> None:
        pass


class HandlerPositive(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, number: int = None) -> None:
        if number > 0:
            print('El número es positivo')


class HandlerNegative(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, number: int = None) -> None:
        if number < 0:
            print('El número es negativo')


class HandlerPositiveRange(HandlerAbstract):
    '''Manejador Concreto'''

    def check(self, number: int = None) -> None:
        if number in range(1, 51):
            print('El número esta entre 1 y 50')


if __name__ == '__main__':
    handler_pos = HandlerPositive()
    handler_neg = HandlerNegative()
    handler_pos_ran = HandlerPositiveRange()

    handler_pos.set_next(handler_neg)
    handler_neg.set_next(handler_pos_ran)

    print('--' * 20)
    handler_pos.process(60)
    print('--' * 20)
    handler_pos.process(-30)
    print('--' * 20)
    handler_pos.process(40)


'''
- En la clase abstracta Handler definimos el método check() que deberán implementar las clases 
a partir de las cuales crearemos los objetos de dicho tipo.

- En dicho método se realizará la comprobación necesaria en cada caso para verificar si el 
objeto actual debe procesar la petición. En caso contrario, se pasará la misma al siguiente 
objeto de la cadena.

- En dicha clase definimos también los métodos 'set' y 'get' para establecer y obtener 
el siguiente objeto al que el actual deberá pasar la petición que ha recibido.

- En el programa principal se crean los objetos que formarán parte de la cadena de 
responsabilidad, utilizando el método set_next() para indicar a cada uno de ellos cuál es 
el siguiente en la misma.
'''
