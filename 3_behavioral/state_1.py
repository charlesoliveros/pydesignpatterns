from abc import ABC, abstractmethod

'''
Este patrón resulta útil cuando necesitamos que un objeto se comporte de forma diferente 
dependiendo del estado interno en el que se encuentre en cada momento.

El objeto cuyo estado es susceptible de cambiar (Contexto) contendrá una referencia a otro 
objeto que define los distintos tipos de estado en que se puede encontrar.

Pero para comprenderlo mejor veamos un sencillo ejemplo en el que controlamos el estado de 
un semáforo:
'''

class TrafficLightStateAbstract(ABC):
    '''State Abstract: estado abstracto'''

    @abstractmethod
    def show(self) -> None:
        '''Método que deberán crear las clases que hereden de ésta'''
        pass


class TrafficLightStateGreen(TrafficLightStateAbstract):
    '''State: Estado concreto'''

    def show(self) -> None:
        print('Luz verde')


class TrafficLightStateYellow(TrafficLightStateAbstract):
    '''State: Estado concreto'''

    def show(self) -> None:
        print('Luz amarilla')


class TrafficLightStateRed(TrafficLightStateAbstract):
    '''State: Estado concreto'''

    def show(self) -> None:
        print('Luz roja')


class TrafficLight:
    '''Context: contexto'''

    traffic_light_state: TrafficLightStateAbstract

    def __init__(self) -> None:
        self.traffic_light_state = TrafficLightStateGreen()

    def set_state(self, traffic_light_state: TrafficLightStateAbstract) -> None:
        self.traffic_light_state = traffic_light_state
    
    def show(self) -> None:
        self.traffic_light_state.show()


if __name__ == '__main__':

    traffic_light = TrafficLight()
    '''Muestra el aviso por defecto (verde, no hay alerta)'''
    traffic_light.show()

    traffic_light.set_state(TrafficLightStateYellow())
    traffic_light.show()

    traffic_light.set_state(TrafficLightStateRed())
    traffic_light.show()


'''
Explicación:

- Al inicio del programa se crea un objeto de tipo TrafficLight (que al ser instanciado quedará 
por defecto en TrafficLightStateGreen) y mostramos el aviso visual correspondiente. 

- Posteriormente cambiamos otras dos veces su estado.

- De este modo se mostrará un aviso u otro dependiendo dependiendo del estado de Semaforo 
en un momento dado.

- Observa que en la propiedad estado actual (traffic_light_state) de Semaforo queda 
registrado el estado del mismo en todo momento.
'''
