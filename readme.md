# ¿Qué son los patrones de diseño?

Cuando se desarrolla una aplicación software es frecuente encontrarse en la situación de tener que volver a 
resolver problemas similares a otros que ya hemos solucionado anteriormente, y debemos volver a hacerlo 
partiendo de cero una y otra y otra vez (incluso dentro del mismo proyecto).

Debido a ello y basándose en la programación orientada a objetos surgieron los patrones de diseño, 
donde cada uno de ellos define la solución para resolver un determinado problema, facilitando además 
la reutilización del código fuente.

Dependiendo de su finalidad pueden ser:

- Patrones de creación: utilizados para crear y configurar de clases y objetos.
- Patrones estructurales: su objetivo es desacoplar las interfaces e implementar clases y objetos. 
Crean grupos de objetos.
- Patrones de comportamiento: se centran en la interacción entre asociaciones de clases y 
objetos definiendo cómo se comunican entre sí.


fuentes: 
- https://informaticapc.com/patrones-de-diseno/