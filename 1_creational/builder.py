from abc import ABC, abstractmethod
from typing import List

'''
Este patrón puede ser utilizado cuando necesitemos crear objetos complejos 
compuestos de varias partes independientes.
'''

class Car:
    '''Product to be built'''

    def __init__(
        self,
        engine: str = '',
        body: str = '',
        air_conditioner: bool = False,
        electric_windows: bool = False
    ) -> None:
        self.engine = engine
        self.body = body
        self.air_conditioner = air_conditioner
        self.electric_windows = electric_windows


class CarBuilderAbstract(ABC):
    '''Abstract Builder'''

    car: Car

    def create_car(self) -> None:
        self.car = Car()

    def get_car(self) -> Car:
        return self.car

    ''' Métodos que deberán ser construídos por las clases que hereden de ésta'''
    @abstractmethod
    def build_engine(self) -> None:
       ...
    
    @abstractmethod
    def build_body(self) -> None:
       ...
    
    @abstractmethod
    def build_air_conditioner(self) -> None:
       ...
    
    @abstractmethod
    def build_electric_windows(self) -> None:
       ...


class CarBuilderBasic(CarBuilderAbstract):
    '''Builder concreto para crear carros basicos'''

    def build_engine(self) -> None:
       self.car.engine = 'Motor de minima potencia'
    
    def build_body(self) -> None:
       self.car.body = 'Carroceria de baja proteccion'
    
    def build_air_conditioner(self) -> None:
       self.car.air_conditioner = False
    
    def build_electric_windows(self) -> None:
       self.car.electric_windows = False


class CarBuilderMedium(CarBuilderAbstract):
    '''Builder concreto para crear carros medium'''

    def build_engine(self) -> None:
       self.car.engine = 'Motor de media potencia'
    
    def build_body(self) -> None:
       self.car.body = 'Carroceria de media proteccion'
    
    def build_air_conditioner(self) -> None:
       self.car.air_conditioner = True
    
    def build_electric_windows(self) -> None:
       self.car.electric_windows = False


class CarBuilderFull(CarBuilderAbstract):
    '''Builder concreto para crear carros full equipo'''

    def build_engine(self) -> None:
       self.car.engine = 'Motor de maxima potencia'
    
    def build_body(self) -> None:
       self.car.body = 'Carroceria de maxima proteccion'
    
    def build_air_conditioner(self) -> None:
       self.car.air_conditioner = True
    
    def build_electric_windows(self) -> None:
       self.car.electric_windows = True


class Director:
    '''
    Clase funciona como capa de abstraccion que permite "construir" un "car" independientemente
    del tipo de carro especifico.. osea del builder concreto.
    '''

    def build_car(self, builder: CarBuilderAbstract) -> None:
        builder.create_car()
        builder.build_engine()
        builder.build_body()
        builder.build_air_conditioner()
        builder.build_electric_windows()


def print_data_cars(car_list: List[Car]) -> None:
    for car in car_list:
        print('-'*50)
        print(
            f'Engine: {car.engine}'
            f'\nBody: {car.body}'
            f'\nAir Conditioner: {car.air_conditioner}'
            f'\nElectric Windows: {car.electric_windows}'
            )


if __name__ == '__main__':
    '''Creamos objeto Director'''
    director = Director()

    '''Crear Concrete Builders'''
    builder_basic = CarBuilderBasic()
    builder_medium = CarBuilderMedium()
    builder_full = CarBuilderFull()

    '''Construir un coche con equipamiento basico'''
    director.build_car(builder_basic)
    car_basic = builder_basic.get_car()

    '''Construir un coche con equipamiento medio'''
    director.build_car(builder_medium)
    car_medium = builder_medium.get_car()

    '''Construir un coche con equipamiento full'''
    director.build_car(builder_full)
    car_full = builder_full.get_car()

    car_list = [car_basic, car_medium, car_full,]

    print_data_cars(car_list)


'''
- En el programa principal creamos el objeto de tipo Director, así como los de tipo 
ConcreteBuilder que le iremos pasando a continuación como parámetro a través de su método 
build_car() (con el fin de crear objetos Coche equipados de diferentes formas).

- Observa que en dicho método se llama a su vez a los diferentes métodos de la 
clase CarBuilderAbstract, mediante los que se crea un nuevo objeto de tipo Coche y se 
componen las diferentes partes del mismo.

- Posteriormente se realiza una llamada al método get_car() del objeto de tipo Director 
para obtener cada objeto de tipo Car, y se muestran sus características.
'''
