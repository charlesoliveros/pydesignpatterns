from abc import ABC, abstractmethod
from typing import Any

'''
Este patrón resulta útil en casos en los que necesitemos crear familias de objetos 
relacionados o dependientes entre sí, sin especificar sus clases concretas. 
'''

'''
A continuación mostramos un ejemplo en el que suponiendo que disponemos de dos familias 
de componentes (compuestas cada una por botones y listas) diferenciadas por el color 
del borde de los mismos (azul y rojo respectivamente), utilizamos este patrón para 
insertar primero los pertenecientes a una familia y después los de la otra.
'''


'''Clases de productos tipo Button'''


class ButtonAbstract(ABC):
    '''Abstract Product'''

    def __init__(self, text: str = None) -> None:
        self.text = text
        self.config()

    def draw(self) -> None:
        print('Dibujando Boton...')
    
    @abstractmethod
    def config(self) -> None:
        '''Método a implementar por las clases que hereden'''
        ...


class ButtonRed(ButtonAbstract):
    '''Concrete Product'''
    
    def config(self) -> None:
        print('Boton con borde Rojo configurado')


class ButtonBlue(ButtonAbstract):
    '''Concrete Product'''
    
    def config(self) -> None:
        print('Boton con borde Azul configurado')


'''Clases de productos tipo Lista'''

class MyListAbstract(ABC):
    '''Abstract Product'''

    def __init__(self) -> None:
        self.elements: list = []
        self.config()

    def add_element(self, element: Any) -> None:
        return self.elements.append(element)

    def get_element(self, position: int) -> Any:
        return self.elements[position]

    def draw(self) -> None:
        print('Dibujando Lista...')
    
    @abstractmethod
    def config(self) -> None:
        '''Método a implementar por las clases que hereden'''
        ...


class MyListRed(MyListAbstract):
    '''Concrete Product'''

    def config(self) -> None:
        print('Lista con borde Roja configurada')


class MyListBlue(MyListAbstract):
    '''Concrete Product'''

    def config(self) -> None:
        print('Lista con borde Azul configurada')


'''Factories'''

class ComponentFactoryAbstract(ABC):
    '''Abstract Factory:
    Clase Principal. Esta fabrica es la capa de abstraccion que permite crear dos tipos 
    de productos concretos diferentes pero relacionadas entre si.
    '''

    @abstractmethod
    def get_bottom(self) -> ButtonAbstract:
        ...

    @abstractmethod
    def get_list(self) -> MyListAbstract:
        ...


class ComponentFactoryRed(ComponentFactoryAbstract):
    '''Concrete Factory:
    Esta fabrica es la capa de abstraccion que permite crear dos tipos de productos concretos
    (Boton y Lista) diferentes pero relacionadas con el color Rojo
    '''

    def get_bottom(self) -> ButtonRed:
        return ButtonRed()

    def get_list(self) -> MyListRed:
        return MyListRed()


class ComponentFactoryBlue(ComponentFactoryAbstract):
    '''Concrete Factory:
    Esta fabrica es la capa de abstraccion que permite crear dos tipos de productos concretos
    (Boton y Lista) diferentes pero relacionadas con el color Azul.
    '''

    def get_bottom(self) -> ButtonBlue:
        return ButtonBlue()

    def get_list(self) -> MyListBlue:
        return MyListBlue()


'''Client'''

class Client:
    '''Client class:
    Usa la Abstract Factory para crear crear dos productos concretos (Boton y Lista), sin saber
    que Concrete Factory va a usar realmente (ComponentFactoryRed o ComponentFactoryBlue) y por
    ende sin saber el tipo concreto de producto va a crear (ButtonRed, ButtonBlue, MyListRed 
    o MyListBlue)
    '''

    def __init__(self, component: ComponentFactoryAbstract) -> None:
        button = component.get_bottom()
        button.draw()

        my_list = component.get_list()
        my_list.draw()


if __name__ == '__main__':

    '''Se insertarán los componentes con el borde rojo'''
    client_red = Client(ComponentFactoryRed())

    '''Se insertarán los componentes con el borde azul'''
    client_blue = Client(ComponentFactoryBlue())


'''
 - En el programa principal creamos instancias de la clase Client, que se encarga de 
 obtener los componentes de la familia indicada y dibujarlos.

- La interface Componentes (la FactoryAbstract) define los métodos que deberán tener 
las clases que la implementen (de tipo Concrete Factory), mediante los cuales devolverán 
los componentes solicitados en cada caso.

- Asimismo las clases abstractas ButtonAbstract y MyListAbstract (de tipo Abstract Product) 
aparte  de implementar métodos comunes, definen otro método que deberán implementar las 
clases que hereden de ellas (será en el que aplicaremos las diferencias entre los 
componentes de ambas familias), el metodo config.. que donde se define el color de cada familia.
'''
