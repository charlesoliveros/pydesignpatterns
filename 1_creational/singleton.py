from __future__ import annotations

'''
Utilizaremos el patrón Singleton cuando por alguna razón necesitemos que exista sólo una 
instancia (un objeto) de una determinada Clase.
Dicha clase se creará de forma que tenga una propiedad estática y un constructor privado, 
así como un método público estático que será el encargado de crear la instancia 
(cuando no exista) y guardar una referencia a la misma en la propiedad estática 
(devolviendo ésta).

En este ejemplo con python usamos una metodo de clase para poder acceder a la variable que 
contiene la instancia.
'''

class Car:

    instance: Car = None

    @classmethod
    def get_instance(cls) -> Car:
        if cls.instance is None:
            cls.instance = Car()
            print('La instancia se ha creado')
        else:
            print('Ya existe la instancia')
        return cls.instance


if __name__ == '__main__':
    for num in range(6):
        instance = Car.get_instance()


'''
Cada vez que se procesa el bucle for se realiza una llamada al método get_instance() de la 
clase Car, devolviéndose siempre una misma instancia (un sólo objeto) de dicha clase 
(que será creada la primera vez que se llame a dicho método).
'''