from abc import ABC, abstractmethod

'''
Podemos utilizar este patrón cuando definamos una clase a partir de la que se crearán 
objetos pero sin saber de qué tipo son, siendo otras subclases las encargadas de decidirlo.

VideoFile y AudioFile son clases concretas de las cuales la clase Factory
retornara sus respectivas instancias.
'''
class MultimediaFileInterface(ABC):

    @abstractmethod
    def play(self) -> None:
        pass


class VideoFile(MultimediaFileInterface):

    def play(self) -> None:
        print('Reproduciendo archivo de Video')


class AudioFile(MultimediaFileInterface):

    def play(self) -> None:
        print('Reproduciendo archivo de Audio')


'''
Clases con la logica de Factory Method:
'''
class FactoryAbstract(ABC):

    VIDEO: str = 'video'
    AUDIO: str = 'audio'

    @classmethod
    @abstractmethod
    def factory_method(cls, type_file: str) -> MultimediaFileInterface:
        ...


class Factory(FactoryAbstract):

    @classmethod
    def factory_method(cls, type_file: str) -> MultimediaFileInterface:

        multimedia_file: MultimediaFileInterface = None
        '''
        En este punto no sabemos si se va a crear una instancia de AudioFIle
        o VideoFile, pero aplicando el principo de "Liskov Substitution" definimos la clase de la
        instancia que se va a retornar usando la clase padre abstracta MultimediaFileInterface
        '''

        if type_file == cls.VIDEO:
            multimedia_file = VideoFile()
        elif type_file == cls.AUDIO:
            multimedia_file = AudioFile()
        
        return multimedia_file


'''
Tener en cuenta que ambos grupos de clases dependen de clases abstractas tanto las
entidades de dominio como las Factory, siendo MultimediaFileInterface clave para  
aplicar el principio de Dependency Inversion y Liskov Substitutio
'''

if __name__ == '__main__':
    video_file = Factory.factory_method(Factory.VIDEO)
    video_file.play()

    audio_file = Factory.factory_method(Factory.AUDIO)
    audio_file.play()
