from __future__ import annotations
from abc import ABC, abstractmethod
from dataclasses import dataclass
from copy import copy


'''
Este patrón nos será útil si necesitamos crear y manipular copias de otros objetos.
Cuando nos disponemos a clonar un objeto es importante tener en cuenta si guarda 
referencias de otros o no. En este punto hay que distinguir las siguientes formas de replicarlos:

- Copia superficial: el objeto clonado tendrá los mismos valores que el original, 
guardando también referencias a otros objetos que contenga (por lo que si son modificados 
desde el objeto original o desde alguno de sus clones el cambio afectará a todos ellos).

- Copia profunda: el objeto clonado tendrá los mismos valores que el original así como copias
 de los objetos que contenga el original (por lo que si son modificados por cualquiera de ellos, 
 el resto no se verán afectados).
'''

'''Ejemplo de copia superficial'''

@dataclass
class EnemyPrototype(ABC):
    '''Clase Prototype:
    Se agrega el constructor dataclass para crear el metodo __repr__ automaticamente.
    '''

    name: str
    weapon: str

    def __init__(self, name: str = '', weapon: str = 'Daga') -> None:
        self.name = name
        self.weapon = weapon

    def clone(self) -> EnemyPrototype:
        '''
        El modulo "annotations" me permite usar metodos y clases aun no definidos,
        como en este caso me permite definir que el metodo va a retornar una instancia
        de la clase actual, que aun no esta totalmente definida en este punto
        '''
        return copy(self)

    '''Métodos que deberán ser construídos por las clases que hereden de ésta'''
    @abstractmethod
    def attack(self) -> None:
        ...

    @abstractmethod
    def stop(self) -> None:
        ...


class Warrior(EnemyPrototype):
    '''Prototipo concreto 1'''

    def attack(self) -> None:
        print('Warrior ataca!')

    def stop(self) -> None:
        print('Warrior se detiene')


class Witcher(EnemyPrototype):
    '''Prototipo concreto 2'''

    def attack(self) -> None:
        print('Witcher ataca!')

    def stop(self) -> None:
        print('Witcher se detiene')



class Client:
    '''
    Cliente de Prototype:
    Es una capa de abstraccion para usar los prototipos concretos
    '''

    '''Para almacenar los objetos de tipo Prototype que se vayan creando'''
    enemies: dict = {}

    def __init__(self) -> None:
        '''
        El constructor del cliente crea los dos primeros enemigos, uno por cada prototipo
        concreto (Warrior y Witcher) y los almacena en el dict
        '''
        enemy1 = Warrior(name='warrior1')
        self.enemies[enemy1.name] = enemy1

        enemy2 = Witcher(name='witcher1')
        self.enemies[enemy2.name] = enemy2

    def add_enemy(self, enemy: EnemyPrototype) -> None:
        self.enemies[enemy.name] = enemy

    def get_enemy(self, enemy_name: str) -> EnemyPrototype:
        return self.enemies.get(enemy_name)

    def get_clon(self, enemy_name: str) -> EnemyPrototype:
        enemy = self.get_enemy(enemy_name)
        if enemy:
            return enemy.clone()
        return None


'''
EnemyPrototype es la clase abstracta que permite aplicar los principios de Liscov Substitution 
y Dependency Inversion, siendo Client la entidad de alto nivel y los prototipos concretos
(Witcher y Warrior) los de bajo nivel.
'''

if __name__ == '__main__':

    client = Client()

    '''Obtenemos el warrior original'''
    warrior1 = client.get_enemy('warrior1')
    print(f'Warrior 1: {warrior1}')

    '''Obtener un segundo warrior (clon del anterior)'''
    warrior2 = client.get_clon(warrior1.name)
    print(f'warrior 2 (clon): {warrior2}')

    '''Modificamos el Warrior clonado'''
    warrior2.name = 'warrior2'
    warrior2.weapon = 'Hacha'

    print('\nDespues de modificar el warrior clonado!!!')
    print(f'Warrior 1: {warrior1}')
    print(f'warrior 2 (clon): {warrior2}')

    '''Modificamos el Warrior original'''
    warrior1.name = 'warrior-1'
    warrior1.weapon = 'Espada'

    print('\nDespues de modificar el warrior original!!!')
    print(f'Warrior 1: {warrior1}')
    print(f'warrior 2 (clon): {warrior2}')
